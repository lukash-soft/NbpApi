package com.example.NbpApi.feature.controller;

import com.example.NbpApi.feature.dto.ComputerDto;
import com.example.NbpApi.feature.dto.InputDto;
import com.example.NbpApi.feature.model.ComputerModel;
import com.example.NbpApi.feature.service.ComputerService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class ComputerController {

    private final ComputerService apiService;

    @GetMapping("/allComputers")
    public List<ComputerModel> getAllComputers(){
        return apiService.getAll();
    }

    @GetMapping("/getComputerByName")
    public List<ComputerModel> getComputerByName(@RequestParam String name) {
        return apiService.getComputerByName(name);
    }

    @GetMapping("/getComputerByAccountingDate")
    public List<ComputerModel> getComputerByDate(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate localDate){
        return apiService.getComputerByDate(localDate);
    }

    @PostMapping("/totalPlnPrice")
    public double countTotalSumOfComputerPriceInPln(@RequestBody InputDto inputDto) throws Exception{
        return apiService.countTotalComputerPriceInPln(inputDto);
    }


}

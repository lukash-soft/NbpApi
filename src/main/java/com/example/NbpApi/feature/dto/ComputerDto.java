package com.example.NbpApi.feature.dto;

import com.example.NbpApi.feature.model.ComputerModel;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComputerDto {

    @JacksonXmlProperty(localName = "nazwa")
    private String name;
    @JacksonXmlProperty(localName = "data_księgowania")
    private String accountingDate;
    @JacksonXmlProperty(localName = "koszt_USD")
    private double usdPrice;
    @JacksonXmlProperty(localName = "koszt_PLN")
    private double plnPrice;

    public static ComputerDto of (ComputerModel computerModel){
        String date = computerModel.getAccountingDate().toString();

        return ComputerDto.builder()
                .name(computerModel.getName())
                .usdPrice(computerModel.getUsdPrice())
                .plnPrice(computerModel.getPlnPrice())
                .accountingDate(date)
                .build();
    }

    public static List<ComputerDto> ofList(List<ComputerModel> computerModelList){
        List<ComputerDto> computerDtos = new ArrayList<>();

        for(ComputerModel computerModel : computerModelList){
            computerDtos.add(ComputerDto.of(computerModel));
        }

        return computerDtos;
    }

}

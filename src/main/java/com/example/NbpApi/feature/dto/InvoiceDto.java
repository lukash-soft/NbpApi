package com.example.NbpApi.feature.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceDto {

    @JacksonXmlProperty(localName = "Komputer")
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<ComputerDto> computerList;
}

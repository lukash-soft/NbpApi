package com.example.NbpApi.feature.dto;

import com.example.NbpApi.feature.model.ComputerModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputDto {
    private LocalDate dateOfUsdPrice;
    private List<ComputerModel> computersList;
}

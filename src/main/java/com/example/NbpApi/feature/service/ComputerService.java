package com.example.NbpApi.feature.service;

import com.example.NbpApi.feature.dto.ComputerDto;
import com.example.NbpApi.feature.dto.InputDto;
import com.example.NbpApi.feature.dto.InvoiceDto;
import com.example.NbpApi.feature.model.ComputerModel;
import com.example.NbpApi.feature.model.CurrencyTable;
import com.example.NbpApi.feature.model.Rate;
import com.example.NbpApi.feature.repository.ComputerRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ComputerService {

    private final RestTemplate restTemplate;
    private final ComputerRepository computerRepository;

    public List<ComputerModel> getAll(){
        //This method is only for testing purposes to get all computers from my h2 DB.
        return computerRepository.findAll();
    }

    public double getUsdPrice(LocalDate usdPriceDate) {
        String url = "http://api.nbp.pl/api/exchangerates/rates/c/usd/" + usdPriceDate + "?format=json";

        CurrencyTable usdCurrencyTable = restTemplate.getForObject(url, CurrencyTable.class);
        List<Rate> rateList = usdCurrencyTable.getRates();
        Rate rate = rateList.get(0);

        return rate.getAsk();
    }

    public double countTotalComputerPriceInPln(InputDto input) throws Exception {
        double usdPriceOfPln = getUsdPrice(input.getDateOfUsdPrice());
        Map<ComputerModel, Double> computerAndPlnPrice = new HashMap<>();
        double result = 0;
        XmlMapper xmlMapper = new XmlMapper();


        for (ComputerModel computer : input.getComputersList()) {
            double plnPrice = computer.getUsdPrice() * usdPriceOfPln;
            plnPrice = roundToTwoDecimalPoints(plnPrice);
            computer.setAccountingDate(input.getDateOfUsdPrice());
            computerAndPlnPrice.put(computer, plnPrice);
            computer.setPlnPrice(plnPrice);
            result += plnPrice;
            computerRepository.save(computer);
        }

        List<ComputerDto> computerDtoList = ComputerDto.ofList(input.getComputersList());

        InvoiceDto invoiceDto = InvoiceDto.builder()
                .computerList(computerDtoList)
                .build();

        xmlMapper.writeValue(new File("src/main/resources/invoice.xml"), invoiceDto);

        return computerAndPlnPrice.values().stream().mapToDouble(v -> v).sum();
    }

    private static double roundToTwoDecimalPoints(double plnPrice) {
        return (double) Math.round(plnPrice * 100) / 100;
    }

    public List<ComputerModel> getComputerByName(String name) {
        return computerRepository.findByNameContains(name);
    }

    public List<ComputerModel> getComputerByDate(LocalDate localDate) {
        return computerRepository.findByAccountingDate(localDate);
    }
}

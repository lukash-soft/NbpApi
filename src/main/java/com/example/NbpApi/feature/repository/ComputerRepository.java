package com.example.NbpApi.feature.repository;

import com.example.NbpApi.feature.model.ComputerModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ComputerRepository extends JpaRepository<ComputerModel, Long> {
    List<ComputerModel> findByAccountingDate(LocalDate accountingDate);

    List<ComputerModel> findByNameContains(String name);
}

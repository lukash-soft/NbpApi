package com.example.NbpApi.feature.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rate {
    private String no;
    private LocalDate effectiveDate;
    private double bid;
    private double ask;
}

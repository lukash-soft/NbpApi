package com.example.NbpApi.feature.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class ComputerModel {

    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @Column
    private LocalDate accountingDate;
    @Column(nullable = false)
    private double usdPrice;
    @Column
    @Builder.Default
    private double plnPrice = 0.0;
}
